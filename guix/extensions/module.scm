;;; Guix-Modules --- Bridging the gap between marvelous communities.
;;; Copyright © 2022, 2023 Ludovic Courtès <ludovic.courtes@inria.fr>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix extensions module)
  #:use-module (gnu packages)
  #:use-module (guix monads)
  #:use-module (guix status)
  #:use-module (guix store)
  #:use-module (guix packages)
  #:use-module (guix modules)
  #:use-module (guix profiles)
  #:use-module (guix derivations)
  #:use-module (guix diagnostics)
  #:use-module (guix gexp)
  #:use-module ((guix self) #:select (make-config.scm))
  #:use-module (guix ui)
  #:use-module (guix i18n)
  #:use-module (guix scripts)
  #:use-module (guix describe)
  #:use-module ((guix transformations)
                #:select (%transformation-options
                          show-transformation-options-help
                          options->transformation
                          manifest-entry-with-transformations))
  #:use-module ((guix scripts build)
                #:select (%standard-build-options
                          show-build-options-help
                          set-build-options-from-command-line))
  #:use-module (guix build utils)
  #:autoload   (gnu packages base) (glibc-locales)
  #:autoload   (gnu packages gnupg) (guile-gcrypt)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-37)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:export (guix-module))

;;; Commentary:
;;;
;;; Convert packages to "environment modules" for use by
;;; <http://modules.sf.net> or <https://lmod.readthedocs.io>.  Modules are
;;; written to "module files", which are Tcl snippets that use the interface
;;; documented at <https://modules.readthedocs.io/en/latest/modulefile.html>.
;;;
;;; Code:

(define not-config?
  ;; Select (guix …) and (gnu …) modules, except (guix config).
  (match-lambda
    (('guix 'config) #f)
    (('guix _ ...) #t)
    (('gnu _ ...) #t)
    (_ #f)))

(define (manifest-entry-with-synopsis-property entry)
  "Return ENTRY with an additional 'package-synopsis' property if its item
is a package"
  (manifest-entry
    (inherit entry)
    (dependencies (map manifest-entry-with-synopsis-property
                       (manifest-entry-dependencies entry)))
    (properties
     (let ((item (manifest-entry-item entry)))
       (if (package? item)
           `((package-synopsis . ,(package-synopsis-string item))
             ,@(manifest-entry-properties entry))
           (manifest-entry-properties entry))))))

(define (manifest-entry-with-category-property entry)
  "Return ENTRY with an extra 'module-category' property."
  (define not-slash
    (char-set-complement (char-set #\/)))

  (define (location->module-category location)
    (match (string-tokenize (location-file location) not-slash)
      ((_ ... "packages" file)
       (basename file ".scm"))
      ((_ ... directory file)
       (string-append directory "/" (basename file ".scm")))
      (_
       #f)))

  (if (assoc-ref (manifest-entry-properties entry) 'module-category)
      entry                                       ;leave unchanged
      (manifest-entry
        (inherit entry)
        (dependencies (map manifest-entry-with-category-property
                           (manifest-entry-dependencies entry)))
        (properties
         (let ((item (manifest-entry-item entry)))
           (if (package? item)
               (match (assoc-ref (package-properties item) 'module-category)
                 (#f
                  (match (package-location item)
                    (#f (manifest-entry-properties entry))
                    (location
                     `((module-category . ,(location->module-category location))
                       ,@(manifest-entry-properties entry)))))
                 (category
                  `((module-category . ,category)
                    ,@(manifest-entry-properties entry))))
               (manifest-entry-properties entry)))))))


(define (manifest->modules manifest)
  "Return a directory with module files for all the packages listed in
MANIFEST."
  (define annotated-manifest
    (map-manifest-entries manifest-entry-with-synopsis-property
                          manifest))

  (define build
    (with-extensions (list guile-gcrypt)
      (with-imported-modules `(((guix config) => ,(make-config.scm))
                               ,@(source-module-closure
                                  '((guix profiles)
                                    (guix utils)
                                    (guix search-paths)
                                    (guix build utils))
                                  #:select? not-config?))
        #~(begin
            (use-modules (guix profiles)
                         ((guix utils) #:select (string-replace-substring))
                         (guix search-paths)
                         (guix build utils)
                         (srfi srfi-1)
                         (ice-9 match)
                         (ice-9 pretty-print))

            (define (provenance-metadata entry)
              ;; Return a string suitable as a Tcl comment providing
              ;; provenance metadata.
              (define (->string sexp)
                (string-replace-substring
                 (call-with-output-string
                   (lambda (port)
                     (pretty-print sexp port #:per-line-prefix "  ")))
                 "\"" "\\\""))

              (define provenance->channel
                ;; Convert a "provenance" sexp as found in manifests to
                ;; channel code.
                (match-lambda
                  (('repository ('version 0) fields ...)
                   `(channel
                     ,@(filter-map
                        (match-lambda
                          (('branch #f) #f)
                          (('name symbol)
                           (list 'name (list 'quote symbol)))
                          (('introduction
                            ('channel-introduction
                             ('version 0)
                             ('commit commit)
                             ('signer fingerprint)))
                           `(introduction
                             (make-channel-introduction
                              ,commit
                              (openpgp-fingerprint ,fingerprint))))
                          (x x))
                        fields)))))

              (define provenance
                ;; Provenance data.  If ENTRY doesn't have it, look for info
                ;; in its parent entry, if any.
                (let loop ((entry entry))
                  (if entry
                      (or (assoc-ref (manifest-entry-properties entry)
                                     'provenance)
                          (loop (force (manifest-entry-parent entry))))
                      '())))

              (define transformations
                (assoc-ref (manifest-entry-properties entry)
                           'transformations))

              (string-append
               "proc ModulesHelp {} {\n"
               "  puts \"This module was generated from a GNU Guix package.\"\n"
               (match provenance
                 (()
                  "  puts \"Uh, oh! Provenance data is unavailable.\"\n")
                 (_
                  (string-append "  puts \"Provenance data (channels):\n\n"
                                 (->string
                                  (cons 'list
                                        (map provenance->channel provenance)))
                                 "\"\n")))

               (match transformations
                 (#f "")
                 (_
                  (string-append "  puts \"Package transformations:\n\n"
                                 (->string transformations)
                                 "\"\n")))
               "}\n"))

            (define (dependency-loading entry)
              ;; Return a string that emits "module load" statements for
              ;; dependencies of ENTRY (propagated inputs).
              (string-concatenate
               (map (lambda (entry)
                      (define spec
                        (module-file-name entry))

                      (format #f "if {![ is-loaded ~a ]} {
  module load ~a~%}\n"
                              spec spec))
                    (manifest-entry-dependencies entry))))

            (define (module-file-name entry)
              (define category
                (assoc-ref (manifest-entry-properties entry)
                           'module-category))

              (string-append (if category
                                 (string-append category "/")
                                 "")
                             (manifest-entry-name entry)
                             (match (manifest-entry-output entry)
                               ("out" "")
                               (output (string-append "-" output)))
                             "/"
                             (manifest-entry-version entry)))

            (define* (entry->module entry port search-paths)
              "Write to PORT a module file for ENTRY."
              (define search-path-statement
                (match-lambda
                  ((search-path . value)
                   (define variable
                     (search-path-specification-variable search-path))
                   (define separator
                     (search-path-specification-separator search-path))

                   (format #f "\
prepend-path -d ~s ~a \"~a\"~%"
                           (or (search-path-specification-separator search-path) "") ;XXX
                           (search-path-specification-variable search-path)
                           value))))

              (format port "#%Module1.0
~a

module-whatis ~s
~a

# Make the right glibc locale data visible.
prepend-path -d \":\" GUIX_LOCPATH \"~a\"

~{~a~}
"
                      (provenance-metadata entry)
                      (or (string-trim-both
                           (assoc-ref (manifest-entry-properties entry)
                                      'package-synopsis))
                          "Package imported from GNU Guix.")
                      (dependency-loading entry)
                      #$(file-append glibc-locales "/lib/locale")
                      (map search-path-statement
                           (evaluate-search-paths
                            (delete-duplicates search-paths)
                            (list (manifest-entry-item entry))))))

            (define profile
              #$(profile (content annotated-manifest)
                         (name "module-profile")
                         (allow-collisions? #t)))

            (define manifest
              (profile-manifest profile))

            (define search-paths
              ;; To simplify things, honor all the search paths that appear in
              ;; MANIFEST.  It's a little bit more than needed--e.g., if you do
              ;; "module load python", you don't need 'C_INCLUDE_PATH' to be
              ;; defined--but that's OK.
              (manifest-search-paths manifest))

            (for-each (lambda (entry)
                        (define file
                          (string-append #$output "/"
                                         (module-file-name entry)))

                        (mkdir-p (dirname file))
                        (call-with-output-file file
                          (lambda (port)
                            (entry->module entry port search-paths))))
                      (manifest-transitive-entries manifest))))))

  (computed-file "modules" build))

(define %gc-roots-directory
  ;; Directory for garbage collector roots created by this tool.
  (string-append %profile-directory "/modules"))

(define %module-gc-root
  ;; The profile that serves as a garbage collector root.
  (string-append %gc-roots-directory "/modules"))

(define (register-module-generation modules)
  "Create a new generation under %MODULE-GC-ROOT and return its file name."
  (define number
    (generation-number %module-gc-root))

  (define name
    (generation-file-name %module-gc-root (+ 1 number)))

  (mkdir-p (dirname name))
  (symlink modules name)
  (switch-symlinks %module-gc-root name)
  (info (G_ "created new module generation at ~a~%") name)
  name)


;;;
;;; Command-line options.
;;;

(define (show-help)
  (display (G_ "Usage: guix module COMMAND [OPTION]...
Provide an \"environment module\" interface for Guix.\n"))
  (newline)
  (display (G_ "Available commands:\n"))
  (display (G_ "
    create    convert packages to module files"))
  (newline)
  (display (G_ "
  -o, --output-directory=DIRECTORY
                         write module files to DIRECTORY"))
  (display (G_ "
      --categorize       categorize module names--e.g., \"maths/pari-gp\""))
  (display (G_ "
  -m, --manifest=FILE    create modules with the manifest from FILE"))
  (display (G_ "
  -n, --dry-run          do not actually build the modules"))
  (newline)

  (show-build-options-help)
  (newline)
  (show-transformation-options-help)
  (newline)

  (display (G_ "
  -h, --help             display this help and exit"))
  (display (G_ "
  -V, --version          display version information and exit"))
  (newline)
  (show-bug-report-information))

(define %options
  (cons* (option '(#\o "output-directory") #t #f
                 (lambda (opt name arg result)
                   (alist-cons 'output-directory arg result)))
         (option '(#\m "manifest") #t #f
                 (lambda (opt name arg result)
                   (alist-cons 'manifest arg result)))
         (option '(#\n "dry-run") #f #f
                 (lambda (opt name arg result)
                   (alist-cons 'dry-run? #t result)))
         (option '("categorize") #f #f
                 (lambda (opt name arg result)
                   (alist-cons 'categorize? #t result)))

         (option '(#\h "help") #f #f
                 (lambda args
                   (show-help)
                   (exit 0)))
         (option '(#\V "version") #f #f
                 (lambda args
                   (show-version-and-exit "guix module")))

         (append %transformation-options
                 %standard-build-options)))

(define %default-options
  `((system . ,(%current-system))
    (substitutes? . #t)
    (offload? . #t)
    (graft? . #t)
    (print-build-trace? . #t)
    (print-extended-build-trace? . #t)
    (multiplexed-build-output? . #t)
    (debug . 0)
    (verbosity . 1)))

(define (user-manifest opts)
  "Return a single manifest corresponding to the options in OPTS, an alist as
returned by 'parse-command-line'."
  (concatenate-manifests
   (cons (specifications->manifest
          (filter-map (match-lambda
                        (('argument . spec) spec)
                        (_ #f))
                      opts))
         (filter-map (match-lambda
                       (('manifest . file)
                        (let ((user-module (make-user-module
                                            '((guix profiles) (gnu)))))
                          (load* file user-module)))
                       (_ #f))
                     opts))))


(define-command (guix-module . args)
  (category packaging)
  (synopsis "convert packages to \"environment modules\"")

  (match args
    (("create" args ...)
     (let* ((opts      (parse-command-line args %options (list %default-options)
                                           #:build-options? #t))
            (dry-run?  (assoc-ref opts 'dry-run?))
            (categorize? (assoc-ref opts 'categorize?))
            (directory (assoc-ref opts 'output-directory))
            (transform (options->transformation opts))
            (manifest  (map-manifest-entries
                        (lambda (entry)
                          (manifest-entry-with-transformations
                           (manifest-entry
                             (inherit entry)
                             (item (transform (manifest-entry-item entry))))))
                        (map-manifest-entries
                         (compose manifest-entry-with-provenance
                                  (if categorize?
                                      manifest-entry-with-category-property
                                      identity))
                         (user-manifest opts)))))
       (unless (or directory dry-run?)
         (leave (G_ "missing output directory; use '-o DIRECTORY' to specify one\n")))

       (let ((count (length (manifest-transitive-entries manifest))))
         (if dry-run?
             (info (N_ "would write modules for ~h package~%"
                       "would write modules for ~h packages~%"
                       count)
                   count directory)
             (info (N_ "creating module for ~h package in ~a...~%"
                       "creating modules for ~h packages in ~a...~%"
                       count)
                   count directory)))

       (with-error-handling
         (with-store store
           (with-status-verbosity (assoc-ref opts 'verbosity)
             (set-build-options-from-command-line store opts)
             (with-build-handler (build-notifier #:use-substitutes?
                                                 (assoc-ref opts 'substitutes?)
                                                 #:verbosity
                                                 (assoc-ref opts 'verbosity)
                                                 #:dry-run? dry-run?)
               (run-with-store store
                 (mlet %store-monad ((drv (lower-object
                                           (manifest->modules manifest))))
                   (mbegin %store-monad
                     (built-derivations (list drv))
                     (munless dry-run?
                       (let ((modules (register-module-generation
                                       (derivation->output-path drv)))
                             (gc-root (string-append
                                       (canonicalize-path (dirname directory))
                                       "/" (basename directory))))
                         (switch-symlinks gc-root modules)
                         (return #t))))))

               (unless dry-run?
                 (info (G_ "wrote ~a modules to ~a~%")
                       (length (manifest-transitive-entries manifest))
                       directory))))))))
    ((or ("-h") ("--help"))
     (show-help)
     (exit 0))
    ((or ("-V") ("--version"))
     (show-version-and-exit "guix git"))
    ((command _ ...)
     (leave (G_ "~a: unknown sub-command~%") command))
    (()
     (leave (G_ "missing sub-command~%")))))

;; Local Variables:
;; eval: (put 'call-with-output-file* 'scheme-indent-function 1)
;; End:
