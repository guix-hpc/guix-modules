# Guix Modules — Bridging community gaps.

Guix-Modules is an extension of [GNU Guix](https://guix.gnu.org) that
allows you to generate [_environment
modules_](http://modules.sourceforge.net), as commonly used in
high-performance computing (HPC) on shared clusters.  Check out the
[introductory blog
post](https://hpc.guix.info/blog/2022/05/back-to-the-future-modules-for-guix-packages/).

> Interested?  Get in touch with us _via_ the
> [`guix-science@gnu.org`](https://hpc.guix.info/about) mailing list if
> you'd like to discuss this.

# Usage

To generate modules to `/tmp/modules` for selected packages, run:

```
GUIX_EXTENSIONS_PATH=$PWD/guix/extensions \
  guix module create -o /tmp/modules \
  coreutils gcc-toolchain python python-numpy
```

Voilà!  The thing will build the packages if they’re not around already
and populate `/tmp/modules` with a bunch of files.  If `/tmp/modules`
already existed, it has been backed up and protected from garbage
collection.

You can then happily load and unload these modules:

```
module use /tmp/modules
module load gcc-toolchain/11.2.0
module load python/3.9.9
```

You can also get provenance data by running:

```
module help gcc-toolchain
```

This shows the list of
[channels](https://guix.gnu.org/manual/en/html_node/Channels.html) as
well as [package transformation
options](https://guix.gnu.org/manual/en/html_node/Package-Transformation-Options.html)
if some were used.

And there’s more!  Like most `guix` commands, you can also pass it a
[manifest](https://guix.gnu.org/manual/en/html_node/Invoking-guix-package.html#index-profile-manifest):

```
GUIX_EXTENSIONS_PATH=$PWD/guix/extensions \
  guix module create -o /tmp/modules \
  -m manifest.scm
```

You can also specify [package transformation
options](https://guix.gnu.org/manual/en/html_node/Package-Transformation-Options.html)
such as `--tune`, and all that.

If flat module hierarchies are not to your taste, you can pass the
`--categorize` option to `guix module create`.  That will lead to module
names such as `maths/pari-gp` and `base/grep` (instead of `pari-gp` and
`grep`).  Default category names are derived from the name of the module
that defines the package at hand—which may or may not be a great choice.

# Rationale

We believe users are better off using Guix directly: it lets them
upgrade when they want to and allows them to [travel in
time](https://guix.gnu.org/manual/en/html_node/Invoking-guix-time_002dmachine.html);
it lets them [customize
packages](https://guix.gnu.org/manual/en/html_node/Package-Transformation-Options.html),
and it lets them [replicate the same
environment](https://guix.gnu.org/manual/en/html_node/Replicating-Guix.html)
elsewhere or at a different point in time.  The [`guix shell`
command](https://guix.gnu.org/manual/devel/en/html_node/Invoking-guix-shell.html)
plays a role comparable to that of `module` and it’s equally fast
[thanks to
caching](https://guix.gnu.org/en/blog/2021/from-guix-environment-to-guix-shell/),
so why bother?

There’s a couple of situations where having modules can be useful.
First, the `module` interface is more “incremental” than that of `guix
shell`: you can “load” and “unload” modules until you obtain the desired
environment, whereas `guix shell` currently expects a list of packages
upfront—no big deal, but some users very much like this incremental
approach.  Second, on some clusters, sysadmins may want to retain
control over resources and disallow the use of Guix by users on compute
nodes.  In that case, they can generate module files and expose them,
along with `/gnu/store`, to compute nodes.  This is much nicer than
building all those modules by hand.
